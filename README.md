#### 介绍
移植导windows下的快速svn导入git工具，并修改支持-B path 只导入svn大仓库中的指定目录

原作者地址:
[git-svn-fast-import](https://travis-ci.org/satori/git-svn-fast-import)

#### 软件架构
软件架构说明


#### 安装教程

1.  安装mingw32环境
2.  ./make.sh

#### 使用说明

1. 原功能 
	$ mkdir -p repo.git && cd repo.git
	$ git init
	$ git-svn-fast-import --stdlayout -r 0:100000 /path/to/svnrepo
	progress Skipped revision 0
	progress Imported revision 1
	progress Imported revision 2
	progress Imported revision 3
	...
	progress Imported revision 99999
	progress Imported revision 100000
	
2. 导指定目录	
	$ git-svn-fast-import -B svndir -r 0:100000 /path/to/svnrepo

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
